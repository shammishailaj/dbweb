module gitea.com/tango/gitea_log_bridge

go 1.12

require (
	code.gitea.io/log v0.0.0-20190526010349-0560851a166a
	gitea.com/lunny/tango v0.6.0
)
